# -*- mode: python -*-

block_cipher = None

added_files = [
                ("images/*.jpg","images"),
                ("images/*.png","."),
                ("tests/*","tests"),
                ("submissions/*","submissions")
              ]
a = Analysis(['window_app.py'],
             pathex=['/home/vlyop/PycharmProjects/TestApp'],
             binaries=[],
             datas=added_files,
             hiddenimports=[],
             hookspath=[],
             runtime_hooks=[],
             excludes=[],
             win_no_prefer_redirects=False,
             win_private_assemblies=False,
             cipher=block_cipher,
             noarchive=False)
pyz = PYZ(a.pure, a.zipped_data,
             cipher=block_cipher)
exe = EXE(pyz,
          a.scripts,
          a.binaries,
          Tree('tests'),
          a.zipfiles,
          a.datas,
          [],
          name='VMC Test APP',
          debug=False,
          bootloader_ignore_signals=False,
          strip=False,
          upx=True,
          runtime_tmpdir=None,
          console=True )
