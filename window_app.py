import base64
import json
import sys
import threading
import glob
import os
# from apscheduler.schedulers.background import BackgroundScheduler
# from apscheduler.triggers import interval
from datetime import datetime, timedelta
from PyQt4 import QtGui, QtCore
from PyQt4.QtWebKit import QWebView
from PyQt4.QtCore import Qt
import requests
from submission import Submission


class Window(QtGui.QMainWindow):

    def __init__(self, parent=None):
        super(Window, self).__init__(parent = parent)
        self.test_question_answers = {}
        self.test = {}
        screen = QtGui.QDesktopWidget().screenGeometry()
        self.test_spent_time = 0
        # self.scheduler = BackgroundScheduler()
        self.is_timer_started = False
        self.width, self.height = screen.width(), screen.height()
        # self.field_width, self.field_height = self.width * 0.01, self.height * 0.03
        self.setGeometry(0, 0, self.width, self.height)
        self.setWindowTitle("PyQT tuts!")
        self.setWindowIcon(QtGui.QIcon('saa.png'))
        self.stackWidget = QtGui.QStackedWidget()
        self.welcomeScreenPage = QtGui.QWidget(self)
        # self.showWelcomeScreen()
        # self.showUploadAnswerPage()
        # self.stackWidget.addWidget(self.welcomeScreenPage)
        # self.stackWidget.addWidget(self.uploadAnswerPage)
        self.screen = 1
        # self.connect(self, QtCore.SIGNAL("quit"), self.onQuit)
        # self.textbox()
        # self.home()
        #
        # window_h_layout =  QtGui.QHBoxLayout(self)
        # window_h_layout.addWidget(self.stackWidget)
        # self.stackWidget.setCurrentIndex(0)
        # self.setLayout(window_h_layout)
        self.screenLauncher()
        self.show()

    def closeEvent(self, event):
        if self.screen == 3 and self.is_timer_started:
            msgBox = QtGui.QMessageBox()
            msgBox.setText("Are you want to close test?Your Test will resume..")
            msgBox.setStandardButtons(QtGui.QMessageBox.No | QtGui.QMessageBox.Yes);
            msgBox.setDefaultButton(QtGui.QMessageBox.Yes);
            if msgBox.exec_() == QtGui.QMessageBox.Yes:
                self.savePauseTest()
                event.accept()
            else:
                event.ignore()
        else:
            event.accept()

    def resource_path(self,relative_path):
        try:
            base_path = sys._MEIPASS
        except Exception:
            base_path = os.path.abspath(".")

        return os.path.join(base_path, relative_path)

    def savePauseTest(self):
        self.check_if_answer_exist()
        if self.is_timer_started:
            self.is_timer_started =False
            # self.scheduler.remove_all_jobs()
        self.testData["time_spent"] = 0
        for testSection in self.testData['testsections']:
            testSection["time_spent"] = 0
            for question in testSection['questions']:
                if question['code'] in self.test_question_answers.keys():
                    if 'student_answer' in self.test_question_answers[question['code']]:
                        question['student_answer'] = self.test_question_answers[question['code']]['student_answer']
                    question['time_spent'] = self.test_question_answers[question['code']]['time_spent']
                    testSection["time_spent"] += self.test_question_answers[question['code']]['time_spent']
            self.testData["time_spent"] += testSection["time_spent"]
        test_json_to_text = json.dumps(self.testData)
        encoded_file_text = base64.b64encode(test_json_to_text.encode('utf-8'))
        answer_file = "./tests/" + self.testData['code'] + ".txt"
        f = open(answer_file, 'wb')
        f.write(encoded_file_text)
        f.close()

    def showWelcomeScreen(self):
        # h_layout = QtGui.QHBoxLayout()
        self.welcomeScreenPage.resize(self.width, self.height)
        label = QtGui.QLabel(self.welcomeScreenPage)
        label.setPixmap(QtGui.QPixmap(self.resource_path("images/loginLogo.jpg")))
        self.welcomeScreenUI()
        vmcLogo = QtGui.QLabel(self.welcomeScreenPage)
        vmc_logo_width, vmc_logo_height = self.width * 0.15, self.height * 0.09
        vmcLogo.resize(vmc_logo_width, vmc_logo_height)
        vmcLogo.move((self.width - vmc_logo_width) / 2,
                     (self.height - self.welcomeScreenPanel.height() - vmc_logo_height) / 2)
        vmcLogo.setPixmap(QtGui.QPixmap(self.resource_path("images/loginlogo1.jpg")))
        self.welcomeScreenPage.show()

    def welcomeScreenUI(self):
        self.welcomeScreenPanel = QtGui.QWidget(self.welcomeScreenPage)
        welcome_screen_panel_width, welcome_screen_panel_height = self.width * 0.5, self.height * 0.5
        self.welcomeScreenPanel.resize(welcome_screen_panel_width, welcome_screen_panel_height)
        self.welcomeScreenPanel.move((self.width - welcome_screen_panel_width) / 2,
                                     (self.height - welcome_screen_panel_height) / 2)

        test_selector_panel = QtGui.QWidget(self.welcomeScreenPanel)
        test_selector_panel.setObjectName("testSelectorPanel")
        test_selector_panel_width = welcome_screen_panel_width * 0.8
        test_selector_panel_height = welcome_screen_panel_height * 0.4
        test_selector_panel.resize(test_selector_panel_width, test_selector_panel_height)
        test_selector_panel.move((welcome_screen_panel_width - test_selector_panel_width) / 2,
                                 (welcome_screen_panel_height - test_selector_panel_height) / 2)
        test_selector_panel.setContentsMargins(0, 25, 0, 25)
        test_selector_panel.setStyleSheet("""
            #testSelectorPanel{
                border: 2px solid black;
            }
        """)

        v_layout = QtGui.QVBoxLayout()
        v_layout.addSpacing(10)
        h_layout_1 = QtGui.QHBoxLayout()

        welcomeLabel = QtGui.QLabel(self.welcomeScreenPanel)
        # self.welcome_v_layout.setSpacing(10)
        # self.welcome_v_layout.setContentsMargins(10,10,0,0)
        # Name Label
        newfont = QtGui.QFont("Times", 15, QtGui.QFont.Bold)
        welcomeLabel.setText('Dear Guest: VidyaMandir Classes Welcomes You')
        welcomeLabel.setFont(newfont)
        welcomeLabel.move(welcome_screen_panel_width * 0.07, welcome_screen_panel_height * 0.235)
        welcomeLabel.setMaximumWidth(test_selector_panel_width * 0.8)
        welcomeLabel.setStyleSheet('color:#f95458')

        selectFileLabel = QtGui.QLabel()
        selectFileLabel.setText('Select Test:')
        selectFileLabel.setMaximumWidth(test_selector_panel_width * 0.2)
        selectFileLabel.setMinimumWidth(test_selector_panel_width * 0.2)
        selectFileLabel.setMinimumHeight(test_selector_panel_height * 0.18)
        selectFileLabel.setAlignment(Qt.AlignCenter)
        selectFileLabel.setStyleSheet('color:#003a7f')

        self.next_login_btn = QtGui.QPushButton()
        self.next_login_btn.setEnabled(False)
        self.next_login_btn.clicked.connect(self.nextLoginBtnClickListener)
        self.next_login_btn.setMinimumHeight(test_selector_panel_height * 0.18)
        self.next_login_btn.setText("Next")

        self.testscomboBox = QtGui.QComboBox()
        self.testscomboBox.setMinimumWidth(test_selector_panel_width * 0.6)
        self.testscomboBox.setMaximumWidth(test_selector_panel_width * 0.6)
        self.testscomboBox.setMinimumHeight(test_selector_panel_height * 0.18)
        self.fetchAndSetTestList()

        h_layout_1.addWidget(selectFileLabel)
        h_layout_1.addWidget(self.testscomboBox)
        h_layout_1.addWidget(self.next_login_btn)

        h_layout_2 = QtGui.QHBoxLayout()
        or_label = QtGui.QLabel("OR")
        or_label.setAlignment(Qt.AlignCenter)
        h_layout_2.addWidget(or_label)

        h_layout_3 = QtGui.QHBoxLayout()
        upload_test_button = QtGui.QPushButton()
        upload_test_button.setMaximumWidth(test_selector_panel_width * 0.3)
        upload_test_button.setMinimumHeight(test_selector_panel_height * 0.18)
        upload_test_button.clicked.connect(self.chooseTestFileClickListener)
        upload_test_button.setText("Upload Test File")
        h_layout_3.addWidget(upload_test_button)
        v_layout.addLayout(h_layout_1)
        v_layout.addLayout(h_layout_2)
        v_layout.addLayout(h_layout_3)

        test_selector_panel.setLayout(v_layout)

        upload_test_answer_panel = QtGui.QWidget(self.welcomeScreenPanel)
        upload_test_answer_panel.setObjectName("uploadTestAnswerPanel")
        upload_test_answer_panel_width = welcome_screen_panel_width * 0.8
        upload_test_answer_panel_height = welcome_screen_panel_height * 0.1
        upload_test_answer_panel.setMinimumWidth(upload_test_answer_panel_width)
        upload_test_answer_panel.setMinimumHeight(upload_test_answer_panel_height)
        upload_test_answer_panel.move((welcome_screen_panel_width - test_selector_panel_width) / 2,
                                      ((welcome_screen_panel_height / 2) + (test_selector_panel_height / 2) + 10))
        # upload_test_answer_panel.setContentsMargins(0, 25, 0, 25)
        upload_test_answer_panel.setStyleSheet("""
                    #uploadTestAnswerPanel{
                        border: 2px solid white;
                        background: #f95458;
                    }
                """)
        v_layout1 = QtGui.QVBoxLayout()
        v_layout1.setAlignment(Qt.AlignCenter)
        upload_test_answer_btn = QtGui.QPushButton()
        upload_test_answer_btn.setText("Upload Answer")
        upload_test_answer_btn.setMaximumWidth(test_selector_panel_width * 0.3)
        upload_test_answer_btn.setMinimumHeight(test_selector_panel_height * 0.18)
        upload_test_answer_btn.clicked.connect(self.showUploadTestAnswerPage)
        self.connection_message_text = QtGui.QLabel()
        self.connection_message_text.setText("OK")
        self.connection_message_text.setVisible(False)
        self.connection_message_text.setAlignment(Qt.AlignCenter)
        self.connection_message_text.setStyleSheet('color:red;')
        v_layout1.addWidget(upload_test_answer_btn)
        v_layout1.addWidget(self.connection_message_text)
        upload_test_answer_panel.setLayout(v_layout1)
        self.welcomeScreenPage.setWindowOpacity(0.6)
        self.welcomeScreenPanel.setStyleSheet("""
                           .QWidget {
                               border: 1px solid gray;
                               border-radius: 5px;
                               background-color:#e2e2d8;
                               font-family: "Open Sans",sans-serif;
                               }
                           """)

    def showUploadTestAnswerPage(self):
        if self.checkInternetConnection():
            self.screen = 2
            self.screenLauncher()
        else:
            self.showMessageDialog("No Internet Connection...")

    def checkInternetConnection(self):
        try:
            _ = requests.get('https://www.google.co.in', timeout=1)
            return True
        except requests.ConnectionError as err:
            return False

    def showMessageDialog(self, message, isSuccess=True):
        self.dialog = QtGui.QDialog()
        self.dialog.resize(self.width * 0.3, self.height * 0.2)
        v_layout = QtGui.QVBoxLayout()
        font = QtGui.QFont("Times", 15, QtGui.QFont.Bold)
        dialog_message_label = QtGui.QLabel()
        dialog_message_label.setText(message)
        dialog_message_label.setAlignment(Qt.AlignCenter)
        dialog_message_label.setStyleSheet('color: red;')
        dialog_message_label.setFont(font)
        v_layout.addWidget(dialog_message_label)
        self.dialog.setLayout(v_layout)
        self.dialog.setWindowTitle("Alert")
        self.dialog.setWindowModality(Qt.ApplicationModal)
        self.dialog.show()

    def nextLoginBtnClickListener(self):
        self.screen = 3
        self.screenLauncher()

    def chooseTestFileClickListener(self):
        self.fileDialog = QtGui.QFileDialog(self)
        self.selectedFilePath = self.fileDialog.getOpenFileName(self.fileDialog, "Choose File", '',
                                                                "Text files (*.txt)")
        self.fileDialog.setLabelText(QtGui.QFileDialog.Accept, "Import")
        if self.selectedFilePath == '':
            print("Please Select File.")
        else:
            self.loadSelectedTestFile()

    def loadSelectedTestFile(self):
        file = open(self.selectedFilePath, "r")
        test_code = os.path.basename(file.name)
        data = file.read()
        new_file = open("./tests/" + test_code, "w+")
        new_file.write(data)
        self.fetchAndSetTestList()

    def fetchAndSetTestList(self):
        dirname, filename = os.path.split(os.path.abspath(__file__))
        if os.path.exists(dirname+"/tests"):
            files = [x for x in os.listdir(dirname+"/tests") if x.endswith(".txt")]
            print(files)
            extracted_test_code = [x.split(".")[0] for x in files]
            extracted_test_code.insert(0, "-- Select Test --")
            self.testscomboBox.clear()
            self.testscomboBox.addItems(extracted_test_code)
            self.testscomboBox.currentIndexChanged.connect(self.proceedToLoginEnabler)
            self.testscomboBox.resize(self.welcomeScreenPanel.width() * 0.5, self.welcomeScreenPanel.height() * 0.06)
        else:
            print("Not Exist")
            os.makedirs(dirname+"/tests")
            self.fetchAndSetTestList()

    def proceedToLoginEnabler(self):
        if not self.testscomboBox.currentText() == "-- Select Test --":
            self.next_login_btn.setEnabled(True)
        else:
            self.next_login_btn.setEnabled(False)

    def showUploadAnswerPage(self):
        self.welcomeScreenPage.hide()
        self.uploadAnswerPage = QtGui.QWidget(self)
        self.uploadAnswerPage.resize(self.width, self.height)
        label = QtGui.QLabel(self.uploadAnswerPage)
        label.setPixmap(QtGui.QPixmap(self.resource_path("images/loginLogo.jpg")))
        vmcLogo = QtGui.QLabel(self.uploadAnswerPage)
        vmcLogo.resize(self.width * 0.15, self.height * 0.09)
        vmcLogo.move(self.width * 0.42, self.height * 0.14)
        vmcLogo.setPixmap(QtGui.QPixmap(self.resource_path("images/loginlogo1.jpg")))
        self.showFileUploadPanel()
        self.uploadAnswerPage.show()

    def showFileUploadPanel(self):
        self.answerUploadPanel = QtGui.QWidget(self.uploadAnswerPage)
        width, height = self.width * 0.3, self.height * 0.3
        self.answerUploadPanel.resize(width, height)
        self.answerUploadPanel.move(self.width * 0.35, self.height * 0.3)

        home_btn = QtGui.QPushButton(self.answerUploadPanel)
        home_btn.setIcon(QtGui.QIcon(self.resource_path("images/home_icon.png")))
        home_btn.resize(width * 0.07, height * 0.1)
        home_btn.setIconSize(QtCore.QSize(width * 0.06, height * 0.08))
        home_btn.clicked.connect(self.gotoWelcomePage)
        home_btn.move(width * 0.07, height * 0.03)
        # Name Label
        self.nameLabel = QtGui.QLabel(self.answerUploadPanel)
        self.nameLabel.setText('Upload Your Answer\'s File:')
        self.nameLabel.move(width * 0.07, height * 0.235)
        self.nameLabel.setStyleSheet('color:#003a7f')

        # Selected File Path Edit Text
        self.selectedFile = QtGui.QLineEdit(self.answerUploadPanel)
        self.selectedFile.setDisabled(True)
        self.selectedFile.resize(width * 0.65, height * 0.1)
        self.selectedFile.move(width * 0.07, height * 0.32)

        # Button To Open File Dialog
        self.chooseFileBtn = QtGui.QPushButton("Choose File", self.answerUploadPanel)
        self.chooseFileBtn.move(width * 0.75, height * 0.32)
        self.chooseFileBtn.resize(width * 0.21, height * 0.1)
        self.chooseFileBtn.clicked.connect(self.chooseFileClickListener)

        # upload File Button (To Server)
        self.uploadBtn = QtGui.QPushButton("Upload Answer", self.answerUploadPanel)
        self.uploadBtn.resize(self.width * 0.08, height * 0.1)
        self.uploadBtn.setVisible(False)
        self.uploadBtn.move(width * 0.07, height * 0.45)
        self.uploadAnswerPage.setWindowOpacity(0.6)
        self.answerUploadPanel.setStyleSheet("""
                   .QWidget {
                       border: 1px solid gray;
                       border-radius: 5px;
                       background-color:#e2e2d8;
                       font-family: "Open Sans",sans-serif;
                       }
                   """)

    def gotoWelcomePage(self):
        self.screen = 1
        self.uploadAnswerPage.hide()
        self.screenLauncher()

    def chooseFileClickListener(self):
        self.fileDialog = QtGui.QFileDialog(self)
        self.selectedFilePath = self.fileDialog.getOpenFileName(self.fileDialog, "Choose File", '',
                                                                "Text files (*.txt)")
        if self.selectedFilePath == '':
            self.selectedFile.setText(self.selectedFilePath)
            self.uploadBtn.setVisible(False)
        else:
            print(self.selectedFilePath)
            self.selectedFile.setText(self.selectedFilePath)
            self.uploadBtn.setVisible(True)
            self.uploadBtn.clicked.connect(self.getFileFromPathAndUpload)

    def getFileFromPathAndUpload(self):
        if self.checkInternetConnection():
            res = Submission().sendAnswerFileToServer(self.selectedFilePath)
            if res == 1:
                self.showMessageDialog("Your answer is successfully submitted.")
            elif res == 2:
                self.selectedFile.setText("")
                self.uploadBtn.setVisible(False)
                self.showMessageDialog("You have already submitted this test.")
            elif res == 0:
                self.showMessageDialog("Server Not Responding!!!Plz Try Later.")
        else:
            self.showMessageDialog("No Internet Connection...")

    def sendAnswerFileToServer(self):
        file = open(self.selectedFilePath, "r")
        answer_file = {'answer': (self.selectedFilePath, file, "multipart/form-data")}
        try:
            res = requests.post("http://localhost:3000/tests/upload_window_app_answer", files=answer_file)
            if res.status_code == 200:
                return 1
            else:
                return 2
        except requests.ConnectionError as err:
            return 0

    def checkTestTime(self):
        # th = threading.Timer(1.0, self.checkTestTime)
        # th.start()
        test_end_time = datetime.utcfromtimestamp(self.testData['end_time']).strftime('%Y-%m-%d %H:%M:%S')
        current_time = datetime.now().strftime("%Y-%m-%d %I:%M:%S")
        self.countdown.setText(datetime.now().strftime("%I:%M:%S"))
        self.countdown.show()
        self.countdown.resize(177, self.height * 0.04)
        self.countdown.move(self.width * 0.85, self.height * 0.03)
        if current_time >= test_end_time:
            QtGui.QMessageBox.about(self, "", " time frame elapsed")
            app.exit(0)

    def textbox(self):
        self.loginPage = QtGui.QWidget(self)
        self.loginPage.resize(self.width, self.height)
        label = QtGui.QLabel(self.loginPage)
        label.setPixmap(QtGui.QPixmap(self.resource_path("images/loginLogo.jpg")))
        self.userInfoWidget = QtGui.QWidget(self.loginPage)
        width, height = self.width * 0.3, self.height * 0.3
        self.userInfoWidget.resize(width, height)
        self.userInfoWidget.move(self.width * 0.35, self.height * 0.3)
        self.nameLabel = QtGui.QLabel(self.userInfoWidget)
        self.nameLabel.setText('Name:')
        self.nameLabel.move(width * 0.12, height * 0.235)
        self.namebox = QtGui.QLineEdit(self.userInfoWidget)
        self.namebox.resize(width * 0.60, height * 0.13)
        self.namebox.move(width * 0.338, height * 0.22)

        self.rollNoLabel = QtGui.QLabel(self.userInfoWidget)
        self.rollNoLabel.setText('Roll No:')
        self.rollNoLabel.move(width * 0.12, height * 0.385)
        self.rollNobox = QtGui.QLineEdit(self.userInfoWidget)
        self.rollNobox.resize(width * 0.60, height * 0.13)
        self.rollNobox.move(width * 0.338, height * 0.37)

        self.emailLabel = QtGui.QLabel(self.userInfoWidget)
        self.emailLabel.setText('Email:')
        self.emailLabel.move(width * 0.12, height * 0.535)
        self.emailbox = QtGui.QLineEdit(self.userInfoWidget)
        self.emailbox.resize(width * 0.60, height * 0.13)
        self.emailbox.move(width * 0.338, height * 0.52)

        self.namebox.setText("name")
        self.rollNobox.setText("Roll Number")
        self.emailbox.setText("abc@def.ghi")

        self.proceed_btn = QtGui.QPushButton("Proceed", self.loginPage)
        self.proceed_btn.clicked.connect(self.proceedBtnClickListener)
        self.proceed_btn.resize(self.width * 0.06, self.height * 0.04)
        self.proceed_btn.move((self.width - self.proceed_btn.width()) / 2, self.height * 0.5)

        vmcLogo = QtGui.QLabel(self.loginPage)
        vmcLogo.resize(self.width * 0.15, self.height * 0.09)
        vmcLogo.move(self.width * 0.42, self.height * 0.14)
        vmcLogo.setPixmap(QtGui.QPixmap(self.resource_path("loginlogo1.jpg")))

        self.loginPage.show()
        self.loginPage.setWindowOpacity(0.6)
        self.userInfoWidget.setStyleSheet("""
           .QWidget {
               border: 1px solid gray;
               border-radius: 5px;
               background-color:#e2e2d8;
               font-family: "Open Sans",sans-serif;
               }
           """)

    def proceedBtnClickListener(self):
        self.screen = 4
        self.screenLauncher()

    def home(self):
        self.btn = QtGui.QPushButton("next", self)
        self.btn.clicked.connect(self.screenLauncher)
        self.btn.resize(self.width * 0.06, self.height * 0.04)
        self.btn.move(self.width * 0.57, self.height * 0.62)
        self.show()

    def screenLauncher(self):
        if self.screen == 1:
            self.showWelcomeScreen()
        elif self.screen == 2:
            self.showUploadAnswerPage()
        elif self.screen == 3:
            self.welcomeScreenPage.hide()
            self.textbox()
            # if self.namebox == "" or self.rollNobox.text() == "" or self.emailbox.text() == "":
            #     QtGui.QMessageBox.about(self, "", "Fill all fields...")
            # elif not re.match(r"^[\w\.\+\-]+\@[\w]+\.[a-z]{2,3}$", self.emailbox.text()):
            #     QtGui.QMessageBox.about(self, "", "Enter valid email address")
            # else:
            #     self.hidelogininfo()
            #     self.testCode()
            #     self.getTestData()
            #     self.screen = self.screen + 1
        elif self.screen == 4:
            self.testCode()
            self.getTestData()
            self.testInstruction()
        elif self.screen == 5:
            self.testStart()

    def hidelogininfo(self):
        self.namebox.hide()
        self.rollNobox.hide()
        self.emailbox.hide()
        self.nameLabel.hide()
        self.rollNoLabel.hide()
        self.emailLabel.hide()

    def testCode(self):
        width, height = self.width * 0.33, self.height * 0.23
        # self.btn.move(self.width * 0.60, self.height * 0.58)
        self.userInfoWidget.resize(width, height)
        self.tCodeLabel = QtGui.QLabel(self.userInfoWidget)
        self.tCodeLabel.setText('Test Code : ')
        self.tCodeLabel.move(width * 0.12, height * 0.42)
        self.tCodeLabel.show()
        self.tCodebox = QtGui.QLineEdit(self.userInfoWidget)
        self.tCodebox.resize(width * 0.45, height * 0.21)
        self.tCodebox.move(width * 0.33, height * 0.40)
        self.tCodebox.show()

    def getTestData(self):
        test_id = "921"
        URL = "http://localhost:3000/tests/" + test_id + "/window_app_test"
        # URL = "http://localhost:3000/tests/921"
        # PARAMS = {'institute-id': 2}

        # r = requests.get(url=URL, headers={"institute-id": "2",'access-token': 'effa9aec8624ef3649c6f8b7e843ab6d','application-id': '929622'}, params={'id': '945'})
        # res = requests.get(url=URL, headers={'application-id': '929622'}, params={'id': '945'})
        # print(res)
        # print(self.testscomboBox.currentText())
        res = open("./tests/" + self.testscomboBox.currentText() + ".txt", "r")
        # print(res.read())
        data = res.read()
        res.close()
        # print(data)

        byte_data = base64.b64decode(data)
        # print(data)

        json_data = byte_data.decode('utf8').replace("'", '"')
        # print(json_data)

        # data = data.replace(":", "colon")
        # print(data)

        # print(res[])
        data = json.loads(json_data)
        # print(data)
        # pyqtRemoveInputHook()
        # pdb.set_trace()
        # self.testData = json.loads(res.read())
        # self.testData = ast.literal_eval(res)
        # with open('file.json') as f:
        #     data = json.load(f)
        self.testData = data
        self.test_end_time = datetime.now() + timedelta(seconds=self.testData['duration'])
        # pyqtRemoveInputHook()
        # pdb.set_trace()
        # test_end_time = datetime.utcfromtimestamp(self.testData['end_time']).strftime('%Y-%m-%d %H:%M:%S')
        # current_time = datetime.now().strftime("%Y-%d-%m %I:%M:%S")
        # self.checkTestTime()

        # print(self.testData)

    def testInstruction(self):
        self.loginPage.hide()
        if "time_spent" in self.testData:
            self.loadTestQuestionAnswer()
        else:
            print("New Test")
        # self.testInstructionPage = QtGui.QWidget(self)
        # self.testInstructionPage.setLayout(self.testInstructionUI())
        #
        # self.testInstructionPage.resize(self.width, self.height)
        # self.testInstructionPage.show()
        self.headerWidget = QtGui.QWidget(self)
        self.headerWidget.resize(self.width, self.height * 0.1)
        pic = QtGui.QLabel(self.headerWidget)
        pic.setPixmap(QtGui.QPixmap(self.resource_path("images/vmclogo.jpg")))
        pic.move(20, 6)
        self.countdown = QtGui.QLabel(self.headerWidget)

        pic.resize(self.width * 0.3, self.height * 0.08)


        # self.tCodeLabel.hide()
        # self.tCodebox.hide()
        # self.userInfoWidget.hide()

        self.userInfoWidget = QtGui.QWidget(self)
        instruction_view = QWebView()
        layout = QtGui.QVBoxLayout(self.userInfoWidget)
        width, height = self.width * 0.6, self.height * 0.60
        self.userInfoWidget.resize(width, height)
        html = '<html><body></br><br>'
        html += '<span style="font-size:16px;margin-left:' + str(
            width * 0.40) + ';"><b>Section:</b> </span><span style="text-align:left;font-size:16px;"> ' + str(
            len(self.testData['testsections'])) + '</span></br> '
        html += '<span style="font-size:16px;margin-left:' + str(
            width * 0.40) + ';"><b>Total Questions:</b> </span><span style="text-align:left;font-size:16px;"> ' + str(
            self.testData['question_count']) + '</span></br>'
        html += '<span style="font-size:16px;margin-left:' + str(
            width * 0.40) + ';"><b>Total Marks:</b></span><span style="text-align:left;font-size:16px;">  ' + str(
            self.testData['max_marks']) + '</span></br>'
        html += '<span style="font-size-16px;margin-left:' + str(
            width * 0.40) + ';"><b>Duration:</b></span><span style="text-align:left;font-size:16px;">  ' + str(
            self.testData['duration'] / 60) + ' mins </span></br></br></br></br></br><br>'
        if not self.testData['instructions']:
            self.testData['instructions'] = ""

        html += '<div style="font-size:16px;"><b>Instructions:</b> \n' + self.testData['instructions'] + '</div>'

        html += '</body></html>'
        instruction_view.setHtml(html)
        layout.addWidget(instruction_view)
        self.userInfoWidget.move(self.width * 0.2, self.height * 0.2)
        self.start_test_btn = QtGui.QPushButton("Start Test", self.userInfoWidget)
        self.start_test_btn.clicked.connect(self.testStart)
        self.start_test_btn.move((self.userInfoWidget.width() - self.start_test_btn.width() - 20),
                                 (self.userInfoWidget.height() - self.start_test_btn.height() - 20))
        self.userInfoWidget.show()
        self.headerWidget.show()
        self.screen = 3

        self.userInfoWidget.setStyleSheet("""
                              .QWidget {
                                  border: 1px white;
                                  font-family: "Open Sans",sans-serif;
                                  background-color:#887;
                                  }
                              """)
        self.headerWidget.setStyleSheet("""
           .QWidget {
               border: 1px gray;
               padding: 10px;
               background-color:#4db3a2;
               }
           """)

    def testInstructionHeaderUI(self):
        h_layout = QtGui.QHBoxLayout()
        h_layout.setMargin(0)
        headerWidget = QtGui.QWidget(self)
        headerWidget.resize(self.width, self.height * 0.1)
        pic = QtGui.QLabel(headerWidget)
        pic.setPixmap(QtGui.QPixmap("vmclogo.jpg"))
        pic.move(20, 6)
        self.countdown = QtGui.QLabel(headerWidget)
        pic.resize(self.width * 0.3, self.height * 0.08)
        headerWidget.setStyleSheet("""
                   .QWidget {
                       border: 1px gray;
                       background-color:#4db3a2;
                       }
                   """)
        h_layout.addWidget(headerWidget)
        return h_layout

    def testInstructionUI(self):
        v_layout = QtGui.QVBoxLayout()
        v_layout.setMargin(0)
        v_layout.addLayout(self.testInstructionHeaderUI())
        v_layout.addLayout(self.testUserInstructionsUI())
        return v_layout

    def testUserInstructionsUI(self):
        v_layout = QtGui.QVBoxLayout()
        v_layout.setMargin(20)
        instruction_view = QWebView()
        width, height = self.width * 0.6, self.height * 0.60
        instruction_view.resize(width,height)
        html = '<html><body></br><br>'
        html += '<span style="font-size:16px;margin-left:' + str(
            width * 0.40) + ';"><b>Section:</b> </span><span style="text-align:left;font-size:16px;"> ' + str(
            len(self.testData['testsections'])) + '</span></br> '
        html += '<span style="font-size:16px;margin-left:' + str(
            width * 0.40) + ';"><b>Total Questions:</b> </span><span style="text-align:left;font-size:16px;"> ' + str(
            self.testData['question_count']) + '</span></br>'
        html += '<span style="font-size:16px;margin-left:' + str(
            width * 0.40) + ';"><b>Total Marks:</b></span><span style="text-align:left;font-size:16px;">  ' + str(
            self.testData['max_marks']) + '</span></br>'
        html += '<span style="font-size-16px;margin-left:' + str(
            width * 0.40) + ';"><b>Duration:</b></span><span style="text-align:left;font-size:16px;">  ' + str(
            self.testData['duration'] / 60) + ' mins </span></br></br></br></br></br><br>'
        if not self.testData['instructions']:
            self.testData['instructions'] = ""

        html += '<div style="font-size:16px;"><b>Instructions:</b> \n' + self.testData['instructions'] + '</div>'

        html += '</body></html>'
        instruction_view.setHtml(html)
        v_layout.addWidget(instruction_view)
        return v_layout

    def loadTestQuestionAnswer(self):
        self.test_spent_time = self.testData["time_spent"]
        for testSection in self.testData['testsections']:
            for question in testSection['questions']:
                if 'student_answer' in question:
                    self.test_question_answers.setdefault(question['code'], {})
                    self.test_question_answers[question['code']]["student_answer"] = question['student_answer']
                if 'time_spent' in question:
                    self.test_question_answers.setdefault(question['code'], {})
                    self.test_question_answers[question['code']]['time_spent'] = question['time_spent']

    def hideInstructionScreen(self):
        self.userInfoWidget.hide()

    def testStart(self):
        current_time = int(datetime.now().timestamp())
        if current_time > self.testData["start_time"] and current_time < self.testData["end_time"]:
            # self.showTimer()
            duration_sec = (self.testData["end_time"] - current_time)
            self.duration = self.testData["duration"] if self.testData["duration"] < duration_sec \
                                                        else duration_sec
            self.left_time = 0
            # self.showTimerScheduler = threading.Timer(1.0, self.showTimer)
            # self.questionSpentScheduler = threading.Timer(1.0, self.questionSpentTimer)
            # self.scheduler = BackgroundScheduler()
            # trigger = interval.IntervalTrigger(seconds=1)
            # self.scheduler.add_job(lambda: self.questionSpentTimer(), trigger=trigger, id="question_time_spent_job")
            # self.scheduler.add_job(lambda: self.showTimer(), trigger=trigger, id="test_time_spent_timer")
            self.userInfoWidget.hide()
            self.hideInstructionScreen()
            self.countdown.resize(177, self.height * 0.04)
            self.countdown.move(self.width * 0.85, self.height * 0.03)
            hours = (self.duration -self.test_spent_time) // 3600
            mins = (self.duration - self.test_spent_time - hours * 3600) // 60
            secs = (self.duration - self.test_spent_time - hours * 3600 - mins * 60)
            if hours < 10:
                hours = "0" + str(hours)
            if mins < 10:
                mins = "0" + str(mins)
            if secs < 10:
                secs = "0" + str(secs)

            QtGui.QMessageBox.about(self, "", "Time left to complete the test " +
                                    str(hours) + ":" + str(mins) + ":" + str(secs))
            testNameLabel = QtGui.QLabel(self.headerWidget)
            testNameLabel.setText(self.testData['name'])
            testNameLabel.show()
            testNameLabel.resize(self.width * 0.5, self.height * 0.06)
            testNameLabel.move(self.width * 0.40, self.height * 0.03)
            testNameLabel.setStyleSheet("QLabel {color : white; font-size:55px;}")

            # self.checkTestTime()
            self.countdown.setStyleSheet(
                "QLabel { background-color : white; color : black; font-size:25px;border-radius: 5px;padding: auto;}")
            self.allSection = []
            for x in range(0, len(self.testData['testsections'])):
                name = self.testData['testsections'][x]["name"]
                question = self.testData['testsections'][x]["questions"]
                current_section = {"name": name, "questions": question}
                self.allSection.append(current_section)
            self.questionButtonSideBar()
            self.countdown.show()
            self.question_label = QWebView()
            self.previousButton = QtGui.QPushButton()
            self.previousQuestionCode = ""
            self.btn_grp.buttonClicked.connect(self.getQuestion)
            question_no = self.allSection[0]["questions"][0]['seq']
            self.testFootBar()
            self.getQuestion(QtGui.QPushButton(), question_no)
        else:
            if current_time < self.testData["start_time"]:
                self.showMessageDialog("You Can't Start Test before "+str(datetime.fromtimestamp(self.testData["start_time"])))
            elif current_time > self.testData["end_time"]:
                self.showMessageDialog(
                    "You Can't Start Test after " + str(datetime.fromtimestamp(self.testData["end_time"])))

    def questionSpentTimer(self):
        questionSpentScheduler = threading.Timer(1.0, self.questionSpentTimer)
        questionSpentScheduler.start()
        # self.test_spent_time += 1
        self.test_question_answers.setdefault(self.current_question_code, {})
        self.test_question_answers[self.current_question_code]["time_spent"] = self.test_question_answers[
                                                                                   self.current_question_code].get(
            "time_spent", 0) + 1
        # print(self.test_question_answers[self.current_question_code])

    def showTimer(self):
        showTimerScheduler = threading.Timer(1.0, self.showTimer)
        showTimerScheduler.start()
        duration = self.duration - self.test_spent_time
        if duration>0:
            hours = duration // 3600
            mins = (duration - hours * 3600) // 60
            secs = (duration - hours * 3600 - mins * 60)
            if hours < 10:
                hours = "0" + str(hours)
            if mins < 10:
                mins = "0" + str(mins)
            if secs < 10:
                secs = "0" + str(secs)
            self.countdown.setText(str(hours) + ":" + str(mins) +":" + str(secs))
            self.test_spent_time += 1
        else:
            if self.is_timer_started:
                self.is_timer_started = False
                # self.scheduler.shutdown()
                # self.scheduler.remove_all_jobs()
            self.check_if_answer_exist()
            self.writeUserAnswers()

    def testFootBar(self):
        self.footbarWidget = QtGui.QWidget(self)
        width, height = self.width * 0.62, self.height * 0.093
        self.footbarWidget.resize(width, height)
        self.footbarWidget.move(self.width * 0.32, self.height * 0.82)
        self.footbarWidget.show()
        self.footbarButtons = QtGui.QButtonGroup()
        self.footbarButtons.setExclusive(True)
        buttonText = ['< previous', 'next >', 'submit', 'mark for review']
        for i in range(0, len(buttonText)):
            button = QtGui.QPushButton(buttonText[i], self.footbarWidget)
            if buttonText[i] == 'submit':
                button.resize(self.width * 0.06, self.height * 0.04)
                button.move(width * 0.8, height * 0.3)
            elif buttonText[i] == 'mark for review':
                pass
                button.resize(self.width * 0.09, self.height * 0.04)
                button.move(width * 0.1, height * 0.3)

            else:
                button.resize(self.width * 0.06, self.height * 0.04)
                button.move(width * 0.4 + (i * 110), height * 0.3)
            button.show()
            self.footbarButtons.addButton(button)

        self.footbarButtons.buttonClicked.connect(self.getBtnType)
        self.footbarWidget.setStyleSheet("""
                               .QWidget {
                                   border: 2px solid black;
                                   padding: 10px;
                                   background-color:#4db3a2;
                                   }
                               """)

    def getBtnType(self, btn):
        if btn.text() == '< previous':
            self.getQuestion(btn, self.question_number - 1)
        elif btn.text() == 'next >':
            self.getQuestion(btn, self.question_number + 1)
        elif btn.text() == 'mark for review':
            button = self.btn_grp.buttons()[self.question_number - 1]
            button.setIcon(QtGui.QIcon(self.resource_path('images/markForReview.png')))
        elif btn.text() == 'submit':
            self.submitBtnClickListener()
        else:
            QtCore.QCoreApplication.quit()

    def submitBtnClickListener(self):
        alert_box = QtGui.QMessageBox()
        reply = alert_box.question(self, 'Message',
                                           "Are you want to submit test?", QtGui.QMessageBox.Yes, QtGui.QMessageBox.No)

        if reply == QtGui.QMessageBox.Yes:
            if self.is_timer_started:
                self.is_timer_started = False
                # self.scheduler.shutdown()
                # self.scheduler.remove_all_jobs()
            self.check_if_answer_exist()
            self.writeUserAnswers()
        else:
            alert_box.close()



    def questionButtonSideBar(self):
        # width,height = self.width * 0.23, self.height * 0.7
        self.btn_grp = QtGui.QButtonGroup()
        btn_widget = QtGui.QWidget()
        layout = QtGui.QGridLayout(btn_widget)
        self.btn_grp.setExclusive(True)
        row = 1
        for x in range(0, len(self.allSection)):
            section = QtGui.QLabel()
            section.setStyleSheet('background: #555;color: #fff; padding: 5px')
            if row > 1:
                row += 2

            col = 1
            layout.addWidget(section, row, col, 1, 4)

            section.setText(self.allSection[x]["name"])
            section.show()
            row += 1
            questions = self.allSection[x]["questions"]
            for i in range(0, len(questions)):
                btn = QtGui.QPushButton()
                btn.setText(str(questions[i]["seq"]))
                if 'student_answer' in questions[i] and questions[i]['student_answer'] != '':
                    btn.setStyleSheet("background-color:#04B45F")
                # btn.resize(width*0.020,50)

                if i % 4 == 0:
                    row += 1
                    col = 1
                layout.addWidget(btn, row, col)
                col += 1
                btn.show()
                self.btn_grp.addButton(btn)
        scroll = QtGui.QScrollArea()
        scroll.setWidget(btn_widget)
        scroll.setWidgetResizable(True)
        self.btnLayoutWidget = QtGui.QWidget(self)
        self.btnLayoutWidget.resize(self.width * 0.3, self.height * 0.67)
        self.btnLayoutWidget.move(self.width * 0.0001, self.height * 0.125)
        layout = QtGui.QVBoxLayout(self.btnLayoutWidget)
        layout.addWidget(scroll)
        self.btnLayoutWidget.show()

    def getQuestion(self, button, question_no=0):
        if question_no == 0:
            question_no = int(button.text())

        self.question_number = question_no
        question = 0
        for i in range(0, len(self.allSection)):
            questions = self.allSection[i]['questions']
            if questions[-1]['seq'] >= question_no:
                for j in range(0, len(questions)):
                    if questions[j]['seq'] == question_no:
                        question = questions[j]
                        break

        self.check_if_answer_exist()
        button = self.btn_grp.buttons()[int(question_no - 1)]
        button.setStyleSheet("background-color:#ff7c56")
        self.questionWidget = QtGui.QWidget(self)
        self.drawQuestion(question)
        self.questionWidget.setStyleSheet("""
                       .QWidget {
                           border: 2px solid black;
                           border-radius: 10px;
                           padding: 10px;
                           background-color: rgb(255, 255, 255);
                           }
                       """)

    def check_if_answer_exist(self):
        doc = self.question_label.page().mainFrame().documentElement()
        if len(doc.toPlainText()) == 0:
            return
        question_code = doc.findFirst("h4").attribute("question_code")
        question_no = doc.findFirst("h4").attribute("question_number")
        question_type = doc.toPlainText().split(" ").__getitem__(1)
        if question_type == 'SINGLE_CHOICE':
            user = doc.findFirst("input[name=option]:checked")
            # if not user.hasAttribute("value"):
            #     return
            option = user.attribute('value')
            # if question_code != '' and ((question_code in self.test and option != self.test[question_code]) or not (
            #         question_code in self.test)):
            #     self.test[question_code] = option
            student_answer = option
        if question_type == "FILL_IN_THE_BLANKS":
            student_answer = doc.findFirst("input").attribute("value")
            # if student_answer == '':
            #     return
        elif question_type == "MULTIPLE_CHOICE":
            option_selected = doc.findAll("input[type=checkbox]:checked").toList()
            # if len(option_selected) == 0:
            #     return
            student_selected_answer = list(map(lambda x: x.attribute('value'), option_selected))
            student_answer = ','.join(map(str, student_selected_answer))
        elif question_type == 'MATCH_THE_FOLLOWING':
            is_answered = False
            option_selected = doc.findAll("input[type=checkbox]").toList()
            # if len(option_selected) == 0:
            #     return
            data = {}
            for option in option_selected:
                data.setdefault(option.attribute("row"),[])
                if option.attribute("isChecked") == 'true':
                    is_answered = True
                    data[option.attribute("row")].append(option.attribute("col"))

            student_answer = ""
            for i in range(0, len(data)):
                student_answer += ",".join(data[str(i+1)])
                if (i < len(data) - 1):
                    student_answer += ":"

            if not is_answered:
                student_answer = ''
        button = self.btn_grp.buttons()[int(question_no)]
        if student_answer != '':
            button.setStyleSheet("background-color:#04B45F")
        else:
            button.setStyleSheet("background-color: none")

        self.test_question_answers.setdefault(question_code, {})
        self.test_question_answers[question_code]["student_answer"] = student_answer
        print(self.test_question_answers[question_code])

    def drawQuestion(self, question):
        self.current_question_code = question['code']
        if not self.is_timer_started:
            self.showTimer()
            self.questionSpentTimer()
            self.is_timer_started = True
        question_text = str((question['text']))
        button_number = str(question['seq'] - 1)

        html = '<html><body>'
        html += '<h4 style="" question_number="' + button_number + '" question_code = "' + question[
            'code'] + '">Q-' + str(question['seq']) + ' ' + question[
                    'type'] + '<span style="float:right">  Attempted Questions:' + str(len(self.test)) + '/' + str(
            self.testData['question_count']) + '</span><h4>'
        html += '<h4><span style="color:green">Positive marks:' + str(
            question['positive_marks']) + '</span> / <span style="color:red"> Negative marks: ' + str(
            question['negative_marks']) + '  </h4>'
        html += question_text
        html += '<h5 style="background-color: #555;color: #fff;padding:10px;border-radius:10px;">Options</h5>'
        html += '<div><form>'
        if question['type'] == 'SINGLE_CHOICE':
            html += self.singleChoiceQuestion(question)
        elif question['type'] == 'FILL_IN_THE_BLANKS':
            html += self.fillInTheBlanksQuestion(question)
        elif question['type'] == 'MULTIPLE_CHOICE':
            html += self.multipleChoiceQuestion(question)
        elif question['type'] == 'MATCH_THE_FOLLOWING':
            html += self.matchTheFollowingQuestion(question)
        html += '</form></div></body></html>'

        layout = QtGui.QVBoxLayout()
        self.questionWidget.setLayout(layout)
        self.question_label.setHtml(html)
        layout.addWidget(self.question_label)
        self.question_label.move(20, 20)
        self.questionWidget.show()
        self.questionWidget.resize(self.width * 0.62, self.height * 0.67)
        self.questionWidget.move(self.width * 0.32, self.height * 0.125)

    def singleChoiceQuestion(self, question):
        question_choices = question['questionchoices']
        html = ''
        for i in range(0, len(question_choices)):
            # if question['code'] in self.test and i == int(self.test[question['code']]) - 1:
            choice = question_choices[i]
            if question['code'] in self.test_question_answers.keys() and \
                    "student_answer" in  self.test_question_answers[question["code"]] and \
                    self.test_question_answers[question["code"]]["student_answer"] == str(choice['seq']):
                html += '<label><input type="radio" name="option" value="' + str(i + 1) + '" checked>' + chr(
                    i + 65) + ") " + \
                        question_choices[i][
                            'text'] + '</label><br>'
            else:
                html += '<label><input type="radio" name="option" value="' + str(i + 1) + '">' + chr(i + 65) + ") " + \
                        question_choices[i][
                            'text'] + '</label><br>'
        return html

    def fillInTheBlanksQuestion(self, question):
        inputSetValue_JS = """
            function setValue(e){
                e.setAttribute("value",e.value);
            }
        """
        html = '<script type = "text/javascript" >' + inputSetValue_JS + '</script>'
        if question["code"] in self.test_question_answers.keys() and \
                    "student_answer" in  self.test_question_answers[question["code"]]:
            html += '<label><input type="text" name="answer" onchange="setValue(this);" onkeyup="this.onchange();" ' \
                    'onpaste="this.onchange();" oninput="this.onchange();" placeholder="Enter Your ' \
                    'Answer"' \
                    'value = "' + self.test_question_answers[question["code"]][
                        "student_answer"] + '"></label><br>'
        else:
            html += '<label><input type="text" name="answer" onchange="setValue(this);" onkeyup="this.onchange();" ' \
                    'onpaste="this.onchange();" oninput="this.onchange();" placeholder="Enter Your Answer"></label><br>'
        return html

    def multipleChoiceQuestion(self, question):
        question_choices = question['questionchoices']
        html = ''
        for i in range(0, len(question_choices)):
            choice = question_choices[i]
            if question["code"] in self.test_question_answers.keys() and \
                    "student_answer" in  self.test_question_answers[question["code"]] and \
                    str(choice['seq']) in self.test_question_answers[question["code"]]["student_answer"].split(","):
                html += '<input type="checkbox" name="option" value="' + str(i + 1) + '" checked><label>' + str(
                    chr(i + 65)) + \
                        question_choices[i][
                            'text'] + '</label><br>'
            else:
                html += '<input type="checkbox" name="option" value="' + str(
                    i + 1) + '"><label style="display: inline;">' + str(
                    chr(i + 65)) + ' )' + question_choices[i][
                            'text'] + '</label><br>'
        return html

    def matchTheFollowingQuestion(self, question):
        inputSetCheckedJS = """
            function setCheckedUnchecked(e){
                if(e.getAttribute("ischecked") === "false"){
                    e.setAttribute("ischecked",true);
                }else{
                    e.setAttribute("ischecked",false);
                }
            }
        """
        html = '<script type = "text/javascript" >' + inputSetCheckedJS + '</script>'
        html += self.getMatchTable(question)
        html += self.getAnswerMatchTable(question)
        return html

    def getMatchTable(self, question):
        questionChoices = question['questionchoices']
        left_td = []
        right_td = []
        left_in = 0
        right_in = 0

        for choice in questionChoices:
            text = choice['text']
            if int(choice['match_the_following_position']) == 1:
                el = '<td style="padding-left:10px;">' + str(
                    left_in + 1) + ')<div style="display: inline-block;padding-left:5px;">' + \
                     text + '</div></td>'
                left_in += 1
                left_td.append(el)
            else:
                el = '<td style="padding-left:10px;">' + str(chr(right_in + 65)) + ')' \
                                                                                   '<div style="display: inline-block;padding-left:5px;">' + \
                     text + '</div></td>'
                right_in += 1
                right_td.append(el)
        min_len = len(left_td) if len(left_td) < len(right_td) else len(right_td)
        table_trs = []
        for i in range(0, min_len):
            tr_el = '<tr>' + left_td[i] + right_td[i] + '</tr>';
            table_trs.append(tr_el)
        if len(left_td) != len(right_td):
            if len(left_td) > len(right_td):
                for i in range(len(table_trs), len(left_td)):
                    tr_el = '<tr>' + left_td[i] + '</tr>';
                    table_trs.append(tr_el)
            else:
                for i in range(len(table_trs), len(right_td)):
                    tr_el = '<tr><td></td>' + right_td[i] + '</tr>';
                    table_trs.append(tr_el)
        table = '<table style = "width:100%"'
        for i in range(0, len(table_trs)):
            table += table_trs[i]
        table += '</table>'
        return table

    def getAnswerMatchTable(self, question):
        questionChoices = question['questionchoices']
        left_match_len = 0
        right_match_len = 0
        for choice in questionChoices:
            if int(choice['match_the_following_position']) == 1:
                left_match_len += 1
            else:
                right_match_len += 1
        table = '<table>'
        table += '<tr style="display:block;padding-left:20px;">'
        for i in range(0, right_match_len):
            table += '<td style="display:inline-block;width:20px;padding-right:9px">' + chr(i + 65) + '</td>'
        table += '</tr>'
        if question['code'] in self.test_question_answers.keys() and "student_answer" in self.test_question_answers[question['code']].keys() and len(self.test_question_answers[question['code']]["student_answer"].strip()):
            answer_list = self.test_question_answers[question['code']]['student_answer'].split(":")
            row_div = ''
            for i in range(0,len(answer_list)):
                row = answer_list[i]
                row_div += '<tr style="display:block;"><td>' + str(i + 1) + '</td>'
                if len(row) != 0:
                    row_answer = row.split(",")
                    for j in range(0, right_match_len):
                        if str(j + 1) in row_answer:
                            html = '<td style="diaplay:inline-block; width:20px;"><input type="checkbox" row = "' + str(
                                i+1) \
                                   + '" col="' + str(j+1) + '" ischecked="true" onclick="setCheckedUnchecked(this);" ' \
                                                          'checked/></td> '
                        else:
                            html = '<td style="diaplay:inline-block; width:20px;"><input type="checkbox" row = "' + str(
                                i+1) \
                                   + '" col="' + str(j+1) + '" ischecked="false" onclick="setCheckedUnchecked(' \
                                                          'this);"/></td> '
                        row_div += html
                else:
                    for j in range(0, right_match_len):
                        html = '<td style="diaplay:inline-block; width:20px;"><input type="checkbox" row = "' + str(
                            i + 1) \
                               + '" col="' + str(j + 1) + '" ischecked="false" onclick="setCheckedUnchecked(' \
                                                          'this);"/></td> '
                        row_div += html
            row_div += '</tr>'
            table += row_div
            table += "</table>"
            return table
        for i in range(0, left_match_len):
            row_div = '<tr style="display:block;"><td>' + str(i + 1) + '</td>'
            for j in range(0, right_match_len):
                html = '<td style="diaplay:inline-block; width:20px;"><input type="checkbox" row = "' + str(i+1) \
                       + '" col="' + str(j+1) + '" ischecked="false" onclick="setCheckedUnchecked(this);"/></td>'
                row_div += html
            row_div += '</tr>'
            table += row_div
        table += "</table>"
        return table

    def writeUserAnswers(self):
        self.student_id = "1"
        self.testData["time_spent"] = 0
        for testSection in self.testData['testsections']:
            testSection["time_spent"] = 0
            for question in testSection['questions']:
                if question['code'] in self.test_question_answers.keys():
                    if self.test_question_answers[question['code']]['student_answer'] != '':
                        question['student_answer'] = self.test_question_answers[question['code']]['student_answer']
                    question['time_spent'] = self.test_question_answers[question['code']]['time_spent']
                    testSection["time_spent"] += self.test_question_answers[question['code']]['time_spent']
            self.testData["time_spent"] += testSection["time_spent"]
        test_json_to_text = json.dumps(self.testData)
        encoded_file_text = base64.b64encode(test_json_to_text.encode('utf-8'))
        # downloads_folder = os.path.join(os.path.expanduser('~'), 'Downloads')
        dirname, file_name = os.path.split(os.path.abspath(__file__))
        if not os.path.exists(dirname + "/submissions"):
            os.makedirs(dirname + "/submissions")
        answer_file = "submissions/" + self.student_id+"_"+self.testData['code'] + ".txt"
        f = open(answer_file, 'wb')
        f.write(encoded_file_text)
        f.close()
        self.questionWidget.setVisible(False)
        if self.checkInternetConnection():
            self.selectedFilePath = answer_file
            res = Submission().sendAnswerFileToServer(self.selectedFilePath)
            if res == 1:
                self.showMessageDialog("Your answer is successfully submitted.")
            elif res == 2:
                self.showMessageDialog("You have already submitted this test.")
            elif res == 0:
                self.showMessageDialog("Your answer saved.You Can Submit it Later.")
        else:
            self.showMessageDialog("Submit your answer which is saved.")
        self.showWelcomePageAfterSubmit()

    def showWelcomePageAfterSubmit(self):
        self.userInfoWidget.close()
        self.headerWidget.close()
        self.footbarWidget.close()
        self.btnLayoutWidget.close()
        self.screen = 1
        self.screenLauncher()


if __name__ == '__main__':
    app = QtGui.QApplication(sys.argv)
    GUI = Window()
    sys.exit(app.exec_())
