import requests
class Submission:
    def sendAnswerFileToServer(self, selectedFilePath):
        file = open(selectedFilePath, "r")
        answer_file = {'answer': (selectedFilePath, file, "multipart/form-data")}
        try:
            res = requests.post("http://localhost:3000/tests/upload_window_app_answer", files=answer_file)
            if res.status_code == 200:
                return 1
            else:
                return 2
        except requests.ConnectionError as err:
            return 0